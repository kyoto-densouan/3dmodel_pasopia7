# README #

1/3スケールの東芝 PASOPIA7風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 東芝

## 発売時期
- 1983年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%91%E3%82%BD%E3%83%94%E3%82%A2)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/866983169072832512)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/99f3edb395360eb31ddee0f5d8f47315)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia7/raw/55abf9a1209d6850b80f33ebe2c36e5dec588fda/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia7/raw/55abf9a1209d6850b80f33ebe2c36e5dec588fda/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia7/raw/55abf9a1209d6850b80f33ebe2c36e5dec588fda/ExampleImage.png)
